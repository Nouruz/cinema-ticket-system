package com.example.demo.Model;

public enum SeatStatus {

    SEAT_BOOKED, SEAT_NOT_BOOKED;

}
