package com.example.demo.Model;

import javax.persistence.*;

@Entity
@Table
public class Seat {

    @GeneratedValue
    @Id
    private long id;

    private SeatStatus seatStatus;

    private final int price = 14;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SeatStatus getSeatStatus() {
        return seatStatus;
    }

    public void setSeatStatus(SeatStatus seatStatus) {
        this.seatStatus = seatStatus;
    }

    public long getPrice() {
        return price;
    }


}
