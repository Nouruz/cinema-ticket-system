package com.example.demo.Controller;

import com.example.demo.Model.Movie;
import com.example.demo.Model.Seat;
import com.example.demo.Model.SeatStatus;
import com.example.demo.Model.Show;
import com.example.demo.Repository.MovieRepository;
import com.example.demo.Repository.ShowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.websocket.server.PathParam;
import java.util.ArrayList;

@Controller
public class ShowController {

    @Autowired
    ShowRepository showRepository;

    @Autowired
    MovieRepository movieRepository;


    @GetMapping(value = "/movie/{movieId}")
    public String getAllShowsByMovie(Model model, @PathVariable(value = "movieId") Long movieId) {
        model.addAttribute("shows", showRepository.findByMovieId(movieId));
        model.addAttribute("Movie", movieRepository.findById(movieId));
        model.addAttribute("Show", new Show());

        return "movie";
    }

    @GetMapping(value = "/show/{showId}")
    public String getAllSeatsByShow(Model model, @PathVariable(value = "showId") Long showId) {
        ArrayList<Seat> seats = new ArrayList<>();

        for (int i = 0; i < 8; i++) {
            Seat seat = new Seat();
            seat.setSeatStatus(SeatStatus.SEAT_NOT_BOOKED);
            seats.add(seat);
        }
        model.addAttribute("seats", seats);
        model.addAttribute("Movie", showRepository.findById(showId));

        return "show";
    }

    @GetMapping(value = "/shows/{movieId}")
    public String getAllShowsByMovdie(Model model, @PathVariable("movieId") Long movieId) {
        Movie movie = movieRepository.findById(movieId).get();
        model.addAttribute("Show", new Show());
        model.addAttribute("Movie", movie);
        return "shows";
    }

    @PostMapping(value = "/shows/{movieId}")
    public Show createShowForMovie(@ModelAttribute("Show") Show show, @PathVariable("movieId") Long movieId) {
        Movie movie = movieRepository.findById(movieId).get();
        show.setMovie(movie);
        return showRepository.save(show);
    }
}


