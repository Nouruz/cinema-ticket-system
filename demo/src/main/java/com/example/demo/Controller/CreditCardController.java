package com.example.demo.Controller;

import com.example.demo.Model.CreditCard;
import com.example.demo.Repository.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CreditCardController {

    @Autowired
    CreditCardRepository creditCardRepository;

    @PostMapping("/zahlen")
    public String pay(@ModelAttribute("CreditCard") CreditCard creditCard) {
        creditCardRepository.save(creditCard);
        return "redirect:/";
    }

    @GetMapping("/zahlen")
    public String payments(Model model) {
        model.addAttribute("CreditCard", new CreditCard());
        return "zahlen";
    }


}
