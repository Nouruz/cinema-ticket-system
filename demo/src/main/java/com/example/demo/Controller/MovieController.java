package com.example.demo.Controller;

import com.example.demo.Model.Day;
import com.example.demo.Model.Movie;
import com.example.demo.Model.Seat;
import com.example.demo.Repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

import static com.example.demo.Model.SeatStatus.SEAT_BOOKED;
import static com.example.demo.Model.SeatStatus.SEAT_NOT_BOOKED;

@Controller
public class MovieController {

    @Autowired
    MovieRepository movieRepository;

    @PostMapping("/movies")
    public Movie createMovie(@ModelAttribute("Movie") Movie movie) {
        return movieRepository.save(movie);

    }

    @GetMapping("/movies")
    public String displayMovie(Model model) {
        model.addAttribute("Movie", new Movie());
        model.addAttribute("allMovies", movieRepository.findAll());
        return "movies";
    }

    @GetMapping("/movielist")
    public String movieList(Model model) {

        model.addAttribute("Movies", movieRepository.findAll());
        model.addAttribute("Movie", new Movie());

        return "movielist";
    }


}
